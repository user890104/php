FROM php:8.1.3

LABEL maintainer=docker-build@6bez10.info

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Sofia
RUN apt-get update && apt-get install -y zlib1g-dev libpng-dev libxml2-dev libxslt1-dev libzip-dev unzip wget

RUN docker-php-ext-install bcmath gd intl soap xsl zip
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q | php -- --quiet --install-dir /usr/local/bin --filename composer
RUN apt-get clean autoclean -y && apt-get autoremove -y && rm -rf /var/lib/apt/lists/*
